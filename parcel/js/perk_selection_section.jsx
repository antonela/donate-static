import React, {useState} from 'react';

import {Checkbox} from './checkbox';
import {PerkTiles} from './perk_tiles';

export function PerkSelectionSection(props) {
  const {displayPerkSelection, noPerk, onNoPerkCheckboxChange, perks, onPerkSelection, selectedPrice, selectedPerk, setPerkOption, perkOption, frequency} = props;

  if (!displayPerkSelection) {
    return null
  } else {
    return (
      <React.Fragment>
        <div className="perk-intro">
          <h2 className="perk-title">Choose your gift as a token of our thanks.</h2>
        </div>
        <div className="no-perk-area">
          <Checkbox name="noPerkCheckbox" checked={noPerk} onChange={onNoPerkCheckboxChange}/>
          <label htmlFor="noPerkCheckbox">No thanks, I don't want a gift. I would prefer 100% of my donation to go to the Tor Project's work.</label>
        </div>
        <PerkTiles
          perks={perks}
          noPerk={noPerk}
          onPerkSelection={onPerkSelection}
          selectedPrice={selectedPrice}
          selectedPerk={selectedPerk}
          setPerkOption={setPerkOption}
          perkOption={perkOption}
          frequency={frequency}
        />
    </React.Fragment>
    );
  }
}
