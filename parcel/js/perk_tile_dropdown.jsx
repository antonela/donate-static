import React from 'react';

export function PerkTileDropdown(props) {
  const {options, setPerkOption, selectedPerk, perk} = props;

  const onChange = (event) => {
    setPerkOption(event.target.value);
  };

  if (options === null) {
    return null;
  } else if (options.length < 2) {
    return null;
  } else {
    const optionElements = options.map(variant =>
      <option key={variant.name} value={variant.name}>{variant.friendlyName}</option>
    );
    return (
      <select name={perk.name} className="perk-sub-select field input" onChange={onChange}>
        {optionElements}
      </select>
    );
  }
}
