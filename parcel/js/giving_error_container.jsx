import React from 'react';

export function GivingErrorContainer(props) {
  const {errors} = props;

  if (errors.length == 0) {
    return null;
  }

  const errorMessages = [];
  for (const error of errors) {
    errorMessages.push(<p key={error.id} className="error perk-desc">{error.message}</p>);
  }

  return (
    <div className="error-container">
      <div className="title">error</div>
      {errorMessages}
    </div>
  );
}
