export function isValidEmail(email) {
  if (isBlank(email)) {
    return false;
  }
  return (email.includes('@') && !email.includes(','));
}

export function isBlank(value) {
  if (value === null) {
    return true;
  }
  if (value.trim() == '') {
    return true;
  }
  return false;
}

export function isNotBlank(value) {
  return !isBlank(value);
}
